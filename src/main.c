#include <stdio.h>
#include "mem_internals.h"
#include "mem.h"

void testAllocate(void* heap) {
    debug_heap(stdout, heap);
    void* block = _malloc(24);
    debug_heap(stdout, heap);
    _free(block);
    fprintf(stdout, "Память успешно аллоцирована:\n");
}

void testFree(void* heap) {
    debug_heap(stdout, heap);
    void* first_block = _malloc(24);
    void* second_block = _malloc(25);
    debug_heap(stdout, heap);
    _free(second_block);
    debug_heap(stdout, heap);
    _free(first_block);
    fprintf(stdout, "Память успешно освобождена:\n");

}

void testFreeNonFirstBlock(void* heap) {
    debug_heap(stdout, heap);
    void* first_block = _malloc(66);
    void* second_block = _malloc(67);
    void* third_block = _malloc(68);
    debug_heap(stdout, heap);
    _free(third_block);
    _free(second_block);
    fprintf(stdout, "Память не первых блоков успешно освобождена:\n");
    debug_heap(stdout, heap);
    _free(first_block);
}

void testBigBlockExtend(void* heap) {
    debug_heap(stdout, heap);
    void* too_much = _malloc(9000);
    debug_heap(stdout, heap);
    _free(too_much);
    debug_heap(stdout, heap);
    fprintf(stdout, "Память для большого блока расширяющего старый регион успешно аллоцирована и освобождена\n");

}

void testBigBlockNew(void* heap) {
    debug_heap(stdout,heap);
    mmap((uint8_t*)heap+5*4096, 1000000, PROT_READ | PROT_WRITE, MAP_PRIVATE | 0x20 | MAP_FIXED, 0, 0 );
    void* too_much_again = _malloc(19484924);
    debug_heap(stdout,heap);
    _free(too_much_again);
    fprintf(stdout, "Память для большого блока занимающего много памяти успешно аллоцирована и освобождена\n");
    debug_heap(stdout, heap);
}

int main() {
    void* heap = heap_init(500);
    testAllocate(heap);
    testFree(heap);
    testFreeNonFirstBlock(heap);
    testBigBlockExtend(heap);
    testBigBlockNew(heap);
    return 0;
}
